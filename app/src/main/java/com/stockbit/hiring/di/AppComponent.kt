package com.stockbit.hiring.di

import com.stockbit.local.di.localModule
import com.stockbit.login.di.featureLoginModule
import com.stockbit.remote.di.createRemoteModule
import com.stockbit.repository.di.repositoryModule
import com.stockbit.watchlist.di.featureWatchListModule

val appComponent= listOf(createRemoteModule("https://min-api.cryptocompare.com/data/"), repositoryModule, localModule,
    featureLoginModule,
    mainModule,
    featureWatchListModule)