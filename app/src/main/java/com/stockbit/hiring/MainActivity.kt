package com.stockbit.hiring

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.stockbit.hiring.databinding.ActivityMainBinding
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val mainViewModel : MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        binding.viewmodel = mainViewModel
        binding.lifecycleOwner = this
        setSupportActionBar(binding.mainToolbar)

        configureNavController()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    // ---

    private fun configureNavController() {
        navController = findNavController(R.id.nav_host_fragment)
        navController.addOnDestinationChangedListener(navigationListener)
        appBarConfiguration = AppBarConfiguration(navController.graph)

        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    private val navigationListener =
        NavController.OnDestinationChangedListener{_,destination,_ ->

            Log.d("test","${destination.id} ${navController.currentDestination?.label}")
            when(destination.id){
                R.id.loginFragment -> mainViewModel.switchToolbarLogin(navController.currentDestination?.label.toString())
                R.id.watchlistFragment -> mainViewModel.switchToolbarWatchList("")
            }
        }
}
