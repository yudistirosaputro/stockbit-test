package com.stockbit.hiring

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.stockbit.common.base.BaseViewModel

class MainViewModel : BaseViewModel() {
    private var _showLogo = MutableLiveData<Int>()
    private  var _showTitle = MutableLiveData<Int>()
    private var _title = MutableLiveData<String>()

    val showLogo : LiveData<Int> get() = _showLogo
    val showTitle  : LiveData<Int> get() = _showLogo
    val title : LiveData<String> get() = _title

    init {
        _showLogo.value = View.VISIBLE
        _showTitle.value = View.VISIBLE
        _title.value = "Masuk"
    }

    fun switchToolbarLogin( title :String) {

        _showLogo.value = View.GONE
        _showTitle.value = View.VISIBLE
        _title.value = title
    } 
    
    fun switchToolbarWatchList(title :String){
        _showLogo.value = View.VISIBLE
        _showTitle.value =  View.GONE
        _title.value = title
    }
}