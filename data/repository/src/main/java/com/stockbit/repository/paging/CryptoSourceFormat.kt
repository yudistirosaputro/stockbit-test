package com.stockbit.repository.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.stockbit.local.dao.WatchlistDao
import com.stockbit.model.db.WatchList
import com.stockbit.model.response.DataResponse
import com.stockbit.model.response.WebSocketRequest
import com.stockbit.remote.CryptoDataSource
import com.stockbit.remote.WebSocketProvider
import com.stockbit.remote.util.ResultState
import com.stockbit.repository.utils.Resource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CryptoSourceFormat(
    private val cryptoDatasource: CryptoDataSource, private val dao: WatchlistDao,

    private val webSocketProvider: WebSocketProvider,

    ) : PageKeyedDataSource<Int, WatchList>() {

    var state: MutableLiveData<Resource<String>> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, WatchList>
    ) {
        state.postValue(Resource.refresh(data = null))
        GlobalScope.launch {
            when (val response =
                cryptoDatasource.fetchTopUsersAsync(1, "usd", params.requestedLoadSize)) {
                is ResultState.HasData -> {
                    if (response.responseData.response == null) {
                        val data: List<WatchList> = transFormToEntity(response.responseData.data)

                        GlobalScope.launch {
                            //insert to cache
                            dao.save(data)
                        }

                        callback.onResult(data, null, 2)
                        state.postValue(Resource(Resource.Status.SUCCESS, "success", ""))
                    } else {
                        state.postValue(
                            Resource(
                                Resource.Status.ERROR,
                                error = response.responseData.response,
                                data = null
                            )
                        )
                    }
                }
                is ResultState.ErrorInformation -> state.postValue(
                    Resource(
                        Resource.Status.ERROR,
                        error = response.message,
                        data = null
                    )
                )
                is ResultState.Error -> state.postValue(
                    Resource(
                        Resource.Status.ERROR,
                        error = response.errorMessage,
                        data = null
                    )
                )
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, WatchList>) {

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, WatchList>) {
        state.postValue(Resource.loading(data = null))
        GlobalScope.launch {
            when (val response =
                cryptoDatasource.fetchTopUsersAsync(params.key, "usd", params.requestedLoadSize)) {
                is ResultState.HasData -> {
                    if (response.responseData.response == null) {
                        val data: List<WatchList> = transFormToEntity(response.responseData.data)

                        GlobalScope.launch {
                            //insert to cache
                            dao.save(data)
                        }

                        callback.onResult(data, params.key + 1)
                        state.postValue(Resource(Resource.Status.SUCCESS, "success", ""))
                    }
                }
                is ResultState.ErrorInformation -> state.postValue(
                    Resource(
                        Resource.Status.ERROR,
                        error = response.message,
                        data = null
                    )
                )
                is ResultState.Error -> state.postValue(
                    Resource(
                        Resource.Status.ERROR,
                        error = response.errorMessage,
                        data = null
                    )
                )
            }
        }
    }

    private fun transFormToEntity(data: List<DataResponse>)  =  data.map {
           val listCode :  MutableList<String> = mutableListOf()
            listCode.add("11~${it.coinInfo.name}")

        val webSocketRequest = WebSocketRequest(
            action = "SubAdd",
            subs = listCode
        )
        webSocketProvider.subscribe(webSocketRequest)
            WatchList(
                id = it.coinInfo.id,
                name = it.coinInfo.name,
                fullName = it.coinInfo.fullName,
                price = it.display.idr.price,
                topTierVolume = it.raw.idr.topTierVolume24Hours,
                lastRefreshed = Date()
            )

    }

}