package com.stockbit.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.gson.Gson
import com.stockbit.local.dao.WatchlistDao
import com.stockbit.model.db.WatchList
import com.stockbit.model.response.WebSocketResponse
import com.stockbit.remote.CryptoDataSource
import com.stockbit.remote.WebSocketProvider
import com.stockbit.repository.paging.CryptoSourceFactory
import com.stockbit.repository.paging.CryptoSourceFormat
import com.stockbit.repository.utils.Resource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

interface CryptoRepository {
    suspend fun getTopTier(isConnected: Boolean): LiveData<PagedList<WatchList>>
    fun stateTopTier(): LiveData<Resource<String>>
    fun refresh()
    fun startSocket()
    fun closeSocket()
    fun responseSubscribe() : LiveData<WebSocketResponse>
}

class CryptoRepositoryImpl(
    private val datasource: CryptoDataSource,
    private val dao: WatchlistDao,
    private val webSocketProvider: WebSocketProvider,
    private val dispatchers: AppDispatchers
) : CryptoRepository {
    private lateinit var cryptoSourceFactory: CryptoSourceFactory

    var responseWebScoket: MutableLiveData<WebSocketResponse> = MutableLiveData()
    val gson = Gson()
    override suspend fun getTopTier(isConnected: Boolean): LiveData<PagedList<WatchList>> {
        // if connected get data from server
        return if (isConnected) {

            cryptoSourceFactory = CryptoSourceFactory(datasource, dao,webSocketProvider)
            val config = PagedList.Config.Builder()
                .setPageSize(15)
                .setInitialLoadSizeHint(15)
                .setEnablePlaceholders(true)
                .build()
            val builder = LivePagedListBuilder(cryptoSourceFactory, config).build()
            builder
        } else {
            // handle offline mode if ready

            val factory: DataSource.Factory<Int, WatchList> = dao.getCache()

            val config = PagedList.Config.Builder()
                .setPageSize(15)
                .setInitialLoadSizeHint(15)
                .setEnablePlaceholders(true)
                .build()
            val builder = LivePagedListBuilder(factory, config).build()

            builder
        }
    }

    override fun stateTopTier(): LiveData<Resource<String>> {
        return Transformations.switchMap(
            cryptoSourceFactory.dataSourceLiveData,
            CryptoSourceFormat::state
        )
    }

    override fun refresh() {
        cryptoSourceFactory.dataSourceLiveData.value?.invalidate()
    }

    override fun startSocket() {
        GlobalScope.launch(dispatchers.main) {
            try {
                webSocketProvider.startSocket().consumeEach {
                    if (it.exception == null) {
                        val data = gson.fromJson(it.text, WebSocketResponse::class.java)
                        if(data.TYPE == "11"){
                            responseWebScoket.postValue(data)
                        }
                    } else {
                        println("Error occurred : ${it.exception}")

                    }
                }
            } catch (ex: java.lang.Exception) {
                println("Error occurred : ${ex.message}")

            }
        }
    }

    override fun closeSocket() {
        webSocketProvider.stopSocket()
    }

    override fun responseSubscribe(): LiveData<WebSocketResponse> {
        return responseWebScoket
    }


}