package com.stockbit.repository.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.stockbit.local.dao.WatchlistDao
import com.stockbit.model.db.WatchList
import com.stockbit.remote.CryptoDataSource
import com.stockbit.remote.WebSocketProvider

class CryptoSourceFactory(
    private val cryptoDatasource: CryptoDataSource,
    private val dao: WatchlistDao,
    private val webSocketProvider: WebSocketProvider,
) : DataSource.Factory<Int, WatchList>(){

    val dataSourceLiveData = MutableLiveData<CryptoSourceFormat>()
    override fun create(): DataSource<Int, WatchList> {
        val dataResponse = CryptoSourceFormat(cryptoDatasource,dao,webSocketProvider)
        dataSourceLiveData.postValue(dataResponse)
        return  dataResponse
    }

}