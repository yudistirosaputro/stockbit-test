package com.stockbit.model.response

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("Message")
    val message:String,
    @SerializedName("Data")
    val data :T,
    @SerializedName("Response")
    val  response:String?
)
