package com.stockbit.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "watchlist")
data class WatchList(
    @PrimaryKey
    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("full_name")
    val fullName: String,

    @SerializedName("price")
    var price: String?,

    @SerializedName("top_tier_volume")
    var topTierVolume: String?,

    var lastRefreshed: Date
)