package com.stockbit.model.response

import com.google.gson.annotations.SerializedName

data class DataResponse (
    @SerializedName("CoinInfo")
    val coinInfo: CoinInfo,
    @SerializedName("DISPLAY")
    val display: Display,
    @SerializedName("RAW")
    val raw: Display
        )

data class CoinInfo(
    @SerializedName("Id")
    val id:String,
    @SerializedName("Name")
    val name:String,
    @SerializedName("FullName")
    val fullName:String,
    @SerializedName("ImageUrl")
    val imageUrl:String
)

data class Display(
    @SerializedName("USD")
    val idr:Idr
)

data class Idr(
    @SerializedName("TOPTIERVOLUME24HOUR")
    val  topTierVolume24Hours:String,
    @SerializedName("PRICE")
    val price:String?

)