package com.stockbit.model.response

import com.google.gson.annotations.SerializedName

data class WebSocketResponse(
    @SerializedName("TYPE") var TYPE: String,
    @SerializedName("SYMBOL") var SYMBOL: String?,
    @SerializedName("FULLVOLUME") var FULLVOLUME: String?
)