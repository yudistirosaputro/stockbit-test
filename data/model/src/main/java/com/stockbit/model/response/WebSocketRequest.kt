package com.stockbit.model.response

import com.google.gson.annotations.SerializedName

data class WebSocketRequest(
    @SerializedName("action")
    val action:String,
    @SerializedName("subs") val subs : List<String>
)

