package com.stockbit.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.stockbit.model.ExampleModel
import com.stockbit.model.db.WatchList
import java.util.*

@Dao
abstract class WatchlistDao: BaseDao<WatchList>() {

    @Query("SELECT * FROM WatchList ")
    abstract fun getCache(): DataSource.Factory<Int,WatchList>


    suspend fun save(data: WatchList): Long {
      return  insert(data)
    }

    suspend fun save(datas: List<WatchList>)  {
        insert(datas)
    }
}