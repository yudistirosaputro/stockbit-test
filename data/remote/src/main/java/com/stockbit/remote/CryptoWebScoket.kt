package com.stockbit.remote

import android.util.Log
import com.google.gson.Gson
import com.stockbit.model.response.WebSocketRequest
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString


class CryptoWebScoket : WebSocketListener() {
    val socketEventChannel: Channel<SocketUpdate> = Channel(1000)
    private lateinit var webSocket: WebSocket


    override fun onOpen(webSocket: WebSocket, response: Response) {
            this.webSocket = webSocket
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        GlobalScope.launch {

            socketEventChannel.send(SocketUpdate(text))
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {

        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        socketEventChannel.close()
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        GlobalScope.launch {
            socketEventChannel.send(SocketUpdate(exception = t))
        }
    }

    fun subscribe(data : WebSocketRequest){
        val gson = Gson()
        val stringData = gson.toJson(data)

        println("subscribe $stringData")
        webSocket.send(stringData)
    }

    companion object {
        const val NORMAL_CLOSURE_STATUS = 1000
    }
}




data class SocketUpdate(
    val text: String? = null,
    val byteString: ByteString? = null,
    val exception: Throwable? = null
)