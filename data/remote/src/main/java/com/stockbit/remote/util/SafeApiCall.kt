package com.stockbit.remote.util

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stockbit.model.response.BaseResponse
import com.stockbit.remote.R
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.util.concurrent.TimeoutException

suspend fun <T> safeApiCall(dispatcher: CoroutineDispatcher, apiCall: suspend () -> T): ResultState<T> {

    return withContext(dispatcher) {
        try {
            ResultState.HasData(apiCall.invoke())
        } catch (throwable: Throwable) {
            Log.e("error ", throwable.localizedMessage)
            when (throwable) {
                is ConnectException -> ResultState.Error("Sorry, We\\'re down for maintenance. Please check back soon")
                is TimeoutException -> ResultState.Error("Request timeout, Please make sure you have stable internet connection")

                is HttpException -> {
                    ResultState.ErrorInformation(getErrorMessage(throwable.response()?.errorBody()))
                }
                else -> ResultState.Error("Sorry. There is unknown error on our end.\\nPlease try again later")

            }
        }
    }
}

private fun getErrorMessage(e: ResponseBody?): String {
    val gson = Gson()
    return e.let {
        val response =
            gson.fromJson<BaseResponse<String>>(
                it?.bytes()?.let { it1 -> String(it1) },
                object : TypeToken<BaseResponse<String>>() {}.type)
        response.message
    }
}