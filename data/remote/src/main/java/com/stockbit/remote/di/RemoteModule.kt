package com.stockbit.remote.di

import com.google.gson.GsonBuilder
import com.stockbit.remote.CryptoDataSource
import com.stockbit.remote.CryptoService
import com.stockbit.remote.WebSocketProvider
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


fun createRemoteModule(baseUrl: String) = module {


    factory {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.HEADERS
            })
            .build()
    }

    single {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create()
        Retrofit.Builder()
            .client(get())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }



    factory{ get<Retrofit>().create(CryptoService::class.java) }

    factory { CryptoDataSource(get()) }
    factory { WebSocketProvider(get()) }
}