package com.stockbit.remote

import com.stockbit.model.response.BaseResponse
import com.stockbit.model.response.DataResponse
import com.stockbit.remote.util.ResultState
import com.stockbit.remote.util.safeApiCall
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.WebSocket

/**
 * Implementation of [CryptoService] interface
 */
class CryptoDataSource(private val cryptoService: CryptoService, private  val dispatcher: CoroutineDispatcher = Dispatchers.IO) {

    suspend  fun fetchTopUsersAsync(page:Int,currency:String,limit:Int) : ResultState<BaseResponse<List<DataResponse>>> {
      return  safeApiCall(dispatcher){
            cryptoService.fetchTopListByTopTier(
                page,currency,limit,"Bearer da58e2626309185078f94a94745af2e8edc3c448a26fc784047602b17ec55516"
            )
        }
    }

}