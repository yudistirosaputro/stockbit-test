package com.stockbit.remote.util


sealed class ResultState<out T> {
    class Loading<out T> : ResultState<T>()
    data class HasData<out T>(val responseData: T) : ResultState<T>()
    data class Error(val errorMessage: String) : ResultState<Nothing>()
    data class ErrorInformation(val message:String) : ResultState<Nothing>()
}