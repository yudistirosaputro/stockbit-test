package com.stockbit.remote

import com.stockbit.model.response.WebSocketRequest
import kotlinx.coroutines.channels.Channel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket

class WebSocketProvider(private val socketOkHttpClient : OkHttpClient)  {

    private var _webSocket: WebSocket? = null

    private var _webSocketListener: CryptoWebScoket? = null


    fun startSocket(): Channel<SocketUpdate> =
        with(CryptoWebScoket()) {
            startSocket(this)
            this@with.socketEventChannel
        }

    fun startSocket(webSocketListener: CryptoWebScoket) {
        _webSocketListener = webSocketListener
        _webSocket = socketOkHttpClient.newWebSocket(
            Request.Builder().url("wss://streamer.cryptocompare.com/v2?api_key=da58e2626309185078f94a94745af2e8edc3c448a26fc784047602b17ec55516").build(),
            webSocketListener
        )
    }


    fun subscribe(data : WebSocketRequest){
        _webSocketListener?.subscribe(data)
    }

    fun stopSocket() {
        try {
            _webSocket?.close(NORMAL_CLOSURE_STATUS, null)
            _webSocket = null
            _webSocketListener?.socketEventChannel?.close()
            _webSocketListener = null
        } catch (ex: Exception) {
        }
    }

    companion object {
        const val NORMAL_CLOSURE_STATUS = 1000
    }

}