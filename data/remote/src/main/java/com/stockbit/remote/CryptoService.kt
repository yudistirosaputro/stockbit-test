package com.stockbit.remote

import com.stockbit.model.response.BaseResponse
import com.stockbit.model.response.DataResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CryptoService {

    @GET("top/totaltoptiervolfull")
   suspend fun fetchTopListByTopTier(@Query("lpage") page:Int, @Query("tsym") tSym :String,
                              @Query("limit") limit:Int,
                              @Header("Authorization") authHeader :String ): BaseResponse<List<DataResponse>>

}