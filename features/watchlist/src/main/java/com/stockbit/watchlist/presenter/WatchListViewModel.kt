package com.stockbit.watchlist.presenter

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.stockbit.common.base.BaseViewModel
import com.stockbit.model.db.WatchList
import com.stockbit.model.response.WebSocketResponse
import com.stockbit.repository.AppDispatchers
import com.stockbit.repository.CryptoRepository
import com.stockbit.repository.utils.Resource
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WatchListViewModel(private val cryptoRepository: CryptoRepository,
                         private val dispatchers: AppDispatchers
) : BaseViewModel() {

    var responseData: LiveData<PagedList<WatchList>>? = null

    private var stateSource: LiveData<Resource<String>> = MutableLiveData()
    private val _state = MediatorLiveData<Resource<String>>()
    val state: LiveData<Resource<String>> get() = _state


    private var subscribeSource: LiveData<WebSocketResponse> = MutableLiveData()
    private val _subscribeResponse=  MediatorLiveData<WebSocketResponse>()
    val subscribeResponse: LiveData<WebSocketResponse> get() = _subscribeResponse

    private var isConnected: Boolean = true

    fun getListCrypto(isConnected: Boolean) {
        this.isConnected = isConnected
        viewModelScope.launch {
            responseData = cryptoRepository.getTopTier(isConnected)
        }
    }

    fun onRefresh() = getDataRefresh()


    private fun getDataRefresh() {
        if (isConnected) {
            viewModelScope.launch(dispatchers.main) {
                cryptoRepository.refresh()
                _state.removeSource(stateSource)
                withContext(dispatchers.io) {
                    stateSource = cryptoRepository.stateTopTier()
                }
                _state.addSource(stateSource) {
                    _state.value = it
                }
            }
        } else {
            _state.value = null
        }
    }

    fun connectToWebSocket() {
        cryptoRepository.startSocket()
        viewModelScope.launch(dispatchers.main) {
            _subscribeResponse.removeSource(subscribeSource)
            withContext(dispatchers.io){
                subscribeSource = cryptoRepository.responseSubscribe()
            }
            _subscribeResponse.addSource(subscribeSource){
                _subscribeResponse.value = it
            }
        }
    }



    override fun onCleared() {
        cryptoRepository.closeSocket()
        super.onCleared()
    }
}