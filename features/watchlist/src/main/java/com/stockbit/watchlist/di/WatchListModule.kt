package com.stockbit.watchlist.di

import com.stockbit.watchlist.presenter.WatchListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val  featureWatchListModule = module {
    viewModel { WatchListViewModel(get(),get()) }
}