package com.stockbit.watchlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stockbit.common.base.BaseFragment
import com.stockbit.common.base.BaseViewModel
import com.stockbit.watchlist.databinding.FragmentWatchlistBinding
import com.stockbit.watchlist.presenter.PageListWatchlist
import com.stockbit.watchlist.presenter.WatchListViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class WatchlistFragment : BaseFragment() {

    private val watchListViewModel: WatchListViewModel by viewModel()
    private lateinit var binding : FragmentWatchlistBinding
    private lateinit var adapter :PageListWatchlist
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentWatchlistBinding.inflate(inflater,container,false)
        binding.viewModel = watchListViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = PageListWatchlist()
        binding.listCrypto.adapter = adapter
        watchListViewModel.connectToWebSocket()
        watchListViewModel.getListCrypto(isNetworkConnected())

        watchListViewModel.subscribeResponse.observe(viewLifecycleOwner,{data ->
            val item = adapter.currentList
            item?.find {
                it.name.equals(data.SYMBOL,ignoreCase = true)
            }?.topTierVolume = data.FULLVOLUME
            adapter.submitList(item)
            adapter.notifyDataSetChanged()
        })
        setUpAdapter()

    }

    private fun setUpAdapter() {
        watchListViewModel.responseData?.observe(viewLifecycleOwner, {
            adapter.submitList(it)

        })
    }

    override fun getViewModel(): BaseViewModel = watchListViewModel
}