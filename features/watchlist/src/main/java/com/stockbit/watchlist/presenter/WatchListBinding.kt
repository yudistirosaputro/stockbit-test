package com.stockbit.watchlist.presenter

import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.stockbit.repository.utils.Resource

@BindingAdapter("bind:showWhenLoading")
fun <T> showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
    if (resource != null) view.isRefreshing = resource.status == Resource.Status.REFRESH
    else view.isRefreshing = false
}
