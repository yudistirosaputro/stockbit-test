package com.stockbit.watchlist.presenter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.stockbit.model.db.WatchList
import com.stockbit.model.response.WebSocketResponse
import com.stockbit.watchlist.R
import java.util.*

class PageListWatchlist : PagedListAdapter<WatchList,PageListWatchlist.ViewHolder>(DIFF_CALLBACK) {

    private lateinit var holder: ViewHolder
    private var position = -1

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<WatchList>() {
            override fun areItemsTheSame(oldItem: WatchList, newItem: WatchList): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: WatchList, newItem: WatchList): Boolean {
                return oldItem == newItem
            }

        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_crypto,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        this.holder = holder
        this.position = position
        holder.bindView(getItem(position))
    }



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name :TextView = view.findViewById(R.id.text_code_name)
        val fullName : TextView = view.findViewById(R.id.text_full_name)
        val price : TextView = view.findViewById(R.id.text_price)
        val volume : TextView = view.findViewById(R.id.text_volume
        )
        fun bindView(dataEntity: WatchList?){
            name.text = dataEntity?.name
            fullName.text = dataEntity?.fullName
            price.text = dataEntity?.price
            volume.text = "Volume : ${dataEntity?.topTierVolume}"
        }


    }
}