package com.stockbit.login.di

import com.stockbit.login.LoginViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val featureLoginModule = module {
    viewModel { LoginViewModel() }
}