package com.stockbit.login

import com.stockbit.common.base.BaseViewModel

class LoginViewModel : BaseViewModel() {


    fun goToWatchList() = navigate(
        LoginFragmentDirections.actionLoginFragmentToWatchlistFragment()
    )

}