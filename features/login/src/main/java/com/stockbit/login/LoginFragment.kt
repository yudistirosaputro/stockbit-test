package com.stockbit.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stockbit.common.base.BaseFragment
import com.stockbit.common.base.BaseViewModel
import com.stockbit.login.databinding.FragmentLoginBinding
import org.koin.android.viewmodel.ext.android.viewModel


class LoginFragment : BaseFragment() {


    private val loginViewModel : LoginViewModel by viewModel()
    private lateinit var  binding : FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater,container,false)
        binding.viewmodel = loginViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun getViewModel(): BaseViewModel = loginViewModel

}