package com.stockbit.common.base

data class BaseModel<T>(
    val success: Boolean,
    val message: String,
    val data: T
)